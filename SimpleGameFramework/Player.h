#pragma once
#include "GameObject.h"
#include "GameManager.h"

class Player : public GameObject
{
public:
	Player();
	virtual ~Player();

	virtual GameObjectType GetType()
	{
		return egotPlayer;
	}

	virtual void SaveClassSpecificData(std::ofstream& saveFile);
	virtual void LoadClassSpecificData(std::ifstream& loadFile);
	virtual void ClassSpecificRender(Gdiplus::Graphics & canvas);

public:
	int health = 100;
};


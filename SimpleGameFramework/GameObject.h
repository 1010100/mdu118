#pragma once
#include <list>
#include <iostream>
#include <fstream>

enum GameObjectType
{
	egotBaseGameObject,
	egotPlayer,
	egotEnemy,
	egotWall,
	egotDoor,
	egotKey,
	egotHealth
};

enum EnumInteraction
{
	interactionCollision,
	interactionDoor,
	interactionHarm,
	interactionHeal,
	interactionKey,
	interactionNothing
};

class GameObject
{
public:
	GameObject();
	~GameObject();

	void SaveToFile(std::ofstream& saveFile);
	virtual void SaveClassSpecificData(std::ofstream& saveFile);

	void LoadFromFile(std::ifstream& loadFile);
	virtual void LoadClassSpecificData(std::ifstream& loadFile);

	
	void Update(double deltaTime);

	virtual void Render(Gdiplus::Graphics& canvas);

	///[updated] is this what you meant Iain?
	virtual void ClassSpecificRender(Gdiplus::Graphics& canvas);
	
	/// make class specific render? 

	virtual GameObjectType GetType()
	{
		return egotBaseGameObject;
	}

	virtual EnumInteraction Interact()
	{
		return interactionNothing;
	}

public:
	Vector2i Location;
	std::string Name = "Unknown";
	GameObject* Parent = nullptr;
	std::list<GameObject*> Children;
};


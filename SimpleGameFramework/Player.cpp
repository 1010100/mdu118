#include "stdafx.h"
#include "Player.h"


Player::Player()
{
}

Player::~Player()
{
}

void Player::SaveClassSpecificData(std::ofstream & saveFile)
{
	// this points to the instance that the code is running on

	saveFile << this->health << std::endl;
	// is the same as
	//saveFile << health << std::endl;
}

void Player::LoadClassSpecificData(std::ifstream & loadFile)
{
	loadFile >> health;
}

void Player::ClassSpecificRender(Gdiplus::Graphics & canvas)
{ //fix so it takes Tile size variable
	//DebugLog("attempting to draw player");
	GameFrameworkInstance.DrawRectangle(canvas, AABBi(Vector2i(0, 0), Vector2i(TileSize, TileSize)), true, Gdiplus::Color::Green);
}

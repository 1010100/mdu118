#pragma once
#include "GameManager.h"
#include "GameObject.h"

class Key : public GameObject
{
public:
	Key();
	virtual ~Key();

	virtual GameObjectType GetType()
	{
		return egotKey;
	}

	virtual EnumInteraction Interact()
	{
		return interactionKey;
	}

	virtual void SaveClassSpecificData(std::ofstream& saveFile);
	virtual void LoadClassSpecificData(std::ifstream& loadFile);
	virtual void ClassSpecificRender(Gdiplus::Graphics & canvas);

public:
	bool isKeyUsed;

};


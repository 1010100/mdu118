#include "stdafx.h"
#include "GameManager.h"
#include "Player.h"
#include "GameObject.h"

#include "Wall.h"
#include "Door.h"
#include "Key.h"
#include "Enemy.h"
#include "healthpack.h"

GameManager& GameManager::Instance()
{
	static GameManager instance;

	return instance;
}

GameManager::GameManager()
{
}
GameManager::~GameManager()
{
}

void GameManager::BeginPlay()
{
	levelObjectPtr = new GameObject();
	isPlayMode = false;
}

void GameManager::SaveLevel()
{
	// save level controller
	std::ofstream saveFile("main.level");
	// save out the version number
	saveFile << 1 << std::endl;
	// save out the level object
	levelObjectPtr->SaveToFile(saveFile);
	saveFile.close();
}


void GameManager::LoadLevel(int gameVersion)
{
		std::ifstream levelFile("main.level");
		
		levelFile >> gameVersion;
		if (gameVersion == 1)
		{
			int objectType = 0;
			levelFile >> objectType;

			GameManagerInstance.levelObjectPtr = new GameObject();
			GameManagerInstance.levelObjectPtr->LoadFromFile(levelFile);
		}

		levelFile.close();

		GameObject* foundObject = nullptr;
		for (GameObject* childPtr : levelObjectPtr->Children)
		{
			GameObject& childRef = *childPtr;

			// dynamic cast - used with inheritance
			Player* playerPtr = dynamic_cast<Player*>(childPtr);
			if (playerPtr)
			{
				focusObjectPtr = playerPtr;
			}
		}
}

void GameManager::EndPlay()
{
	delete levelObjectPtr;
	levelObjectPtr = nullptr;
}

void GameManager::Update(double deltaTime)
{
}

void GameManager::Render(Gdiplus::Graphics& canvas, const CRect& clientRect)
{

	// Save the current transformation of the scene
	Gdiplus::Matrix transform;
	canvas.GetTransform(&transform);

	// are we focusing on an object?
	if (isPlayMode && focusObjectPtr)
	{
		levelOffset = Vector2i(clientRect.Width() / 2,clientRect.Height() / 2);
		levelOffset -= focusObjectPtr->Location;
	}

	canvas.TranslateTransform((Gdiplus::REAL)levelOffset.X, (Gdiplus::REAL)levelOffset.Y);

	// Draw all of the objects!

	levelObjectPtr->Render(canvas);

	canvas.ScaleTransform(0.5f, 0.5f);
	canvas.RotateTransform(30.0f);
	canvas.TranslateTransform(200.0f, 200.0f);


}

void GameManager::LeftButtonDown(const Vector2i & location)
{

	if (isPlayMode)
	{
		return;
	}


// this line and changed location to actualLocation below
	Vector2i actualLocation = location - levelOffset;

	// convert to grid coordinates
	int gridX = actualLocation.X / TileSize;
	int gridY = actualLocation.Y / TileSize;


	Vector2i spawnLocation(gridX * TileSize, gridY * TileSize);

	//* TODO: should split this into a function in the game object

	GameObject* foundObject = nullptr;
	for (GameObject* childPtr : levelObjectPtr->Children)
	{
		GameObject& childRef = *childPtr;

		// dynamic cast - used with inheritance
		Player* playerPtr = dynamic_cast<Player*>(childPtr);
		if (playerPtr)
		{
			// this will ONLY run if childPtr is a player
			// if childPtr is NOT a player then 
			// playerPtr is nullptr
		}

		/// [help] how should I be thinking about putting this into GameObject
		// convert the child's location to world space
		// TODO : this should be a function on game object
		Vector2i testLocation = childRef.Location;
		testLocation += levelObjectPtr->Location;

		if (spawnLocation == testLocation)
		{
			foundObject = childPtr;
		}
	}

	if (foundObject)
	{
		// remove the selected object
		levelObjectPtr->Children.remove(foundObject);
		delete foundObject;

		return;
	}
	

	GameObject* newObject = nullptr;
	switch (objectTypeToPlace)
	{
	case egotBaseGameObject:
		newObject = new GameObject();
		break;

	case egotPlayer:
		newObject = new Player();
		focusObjectPtr = newObject;
		DebugLog("focusObjectPtr" << focusObjectPtr);
		break;

	case egotEnemy:
		newObject = new Enemy();
		break;

	case egotWall:
		newObject = new Wall();
		break;

	case egotDoor:
		newObject = new Door();
		break;

	case egotKey:
		newObject = new Key();
		break;

	case egotHealth:
		newObject= new healthpack();
		break;
	}

	newObject->Location = spawnLocation;

	// TODO: This line below should be in your add
	// child function
	newObject->Location -= levelObjectPtr->Location;

	// add the object to the level
	levelObjectPtr->Children.push_back(newObject);
	newObject->Parent = levelObjectPtr;

}

	void GameManager::PlacementSelector(GameObjectType inputTypeToPlace)
	{ //takes data from input, and changes objectTypeToPlace for the Switch on left click creation.
		objectTypeToPlace = inputTypeToPlace;
	}

	//needed?
	void GameManager::CollisionCheck(EnumInteraction interactorHolder)
	{
		InteractionType = interactorHolder;
	}


	void GameManager::ApplyMovement(const Vector2i & movement)
	{
		// if we aren't focusing on an object move the level
		if (!isPlayMode)
		{
			levelOffset -= movement;
		}
		else if (focusObjectPtr) // we have a focus object
		{
			///TEST CODE FOR COLLISIONCHECKER
			GameObject* foundObject = nullptr;
			for (GameObject* childPtr : levelObjectPtr->Children)
			{
				GameObject& childRef = *childPtr;

					///[help] I am certain that I am sure this isn't working 
					Vector2i initalLoc;
					Vector2i initialLoc = focusObjectPtr->Location;

					Vector2i moveCheck = initialLoc + movement;
					Vector2i moveVecMax = (moveCheck.X + TileSize, moveCheck.Y + TileSize); //max boundary point of AABB
					AABBi moveBB = (moveCheck.X, moveCheck.Y, moveVecMax.X, moveVecMax.Y);

				/// [help] how should I be thinking about putting this into GameObject
				// convert the child's location to world space
				// TODO : this should be a function on game object
				Vector2i childLocVec = childRef.Location;
				//childLocVec -= levelObjectPtr->Location;
				AABBi childLocAABB = (childLocVec.X, childLocVec.Y, childLocVec.X + TileSize, childLocVec.Y + TileSize);

				//attempt to determine syntax

				if (!moveBB.Intersects(childLocAABB))
				{
					focusObjectPtr->Location += movement;
					return;
				}
				else if (moveBB.Intersects(childLocAABB))
				{
					DebugLog(childRef.Interact());
					return;
				}

			}
			
		}
	}

	void GameManager::ToggleEdit()
	{
		isPlayMode = !isPlayMode;
		DebugLog("play mode was toggled to " << isPlayMode)
	}

#pragma once
#include "GameObject.h"
#include "GameManager.h"

class Wall : public GameObject
{
public:
	Wall();
	virtual ~Wall();

	virtual GameObjectType GetType()
	{
		return egotWall;
	}

	virtual void SaveClassSpecificData(std::ofstream& saveFile);
	virtual void LoadClassSpecificData(std::ifstream& loadFile);
	virtual	void ClassSpecificRender(Gdiplus::Graphics & canvas);


public:

};


#include "stdafx.h"
#include "Door.h"


Door::Door()
{
}


Door::~Door()
{
}

void Door::SaveClassSpecificData(std::ofstream & saveFile)
{
	//saveFile << doorLocation << std::endl;
}

void Door::LoadClassSpecificData(std::ifstream & loadFile)
{
	//loadFile >> doorLocation;
}

void Door::ClassSpecificRender(Gdiplus::Graphics & canvas)
{
	//fix so it takes Tile size variable
	//DebugLog("attempting to draw Door");
	GameFrameworkInstance.DrawRectangle(canvas, AABBi(Vector2i(0, 0), Vector2i(TileSize, TileSize)), true, Gdiplus::Color::Blue);
}

#include "stdafx.h"
#include "healthpack.h"


healthpack::healthpack()
{
}


healthpack::~healthpack()
{
}


void healthpack::SaveClassSpecificData(std::ofstream & saveFile)
{
	//saveFile << this->healthPackLocation << std::endl;
}

void healthpack::LoadClassSpecificData(std::ifstream & loadFile)
{
	//loadFile >> healthPackLocation;
}

void healthpack::ClassSpecificRender(Gdiplus::Graphics & canvas)
{
	//fix so it takes Tile size variable
	//DebugLog("attempting to draw health pack");
	GameFrameworkInstance.DrawRectangle(canvas, AABBi(Vector2i(0, 0), Vector2i(TileSize, TileSize)), true, Gdiplus::Color::DarkSeaGreen);
}

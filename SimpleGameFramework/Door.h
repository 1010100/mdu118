#pragma once
#include "GameObject.h"
#include "GameManager.h"
#include "Key.h"

class Door : public GameObject
{
public:
	Door();
	virtual ~Door();

	virtual GameObjectType GetType()
	{
		return egotDoor;
	}

	virtual EnumInteraction Interact()
	{
		return interactionDoor;
	}

	virtual void SaveClassSpecificData(std::ofstream& saveFile);
	virtual void LoadClassSpecificData(std::ifstream& loadFile);
	virtual void ClassSpecificRender(Gdiplus::Graphics & canvas);

public:
	bool isLocked = true;
};
#include "stdafx.h"
#include "GameObject.h"
#include "Player.h"
#include "Wall.h"
#include "Door.h"
#include "Key.h"
#include "Enemy.h"
#include "healthpack.h"
#include "GameManager.h"



GameObject::GameObject()
{
	/*
	// TODO: should split this into a function in the game object
	//from game manager
	GameObject* foundObject = nullptr;
	for (GameObject* childPtr : GameManagerInstance.levelObjectPtr->Children)
	{
		GameObject& childRef = *childPtr;

		// dynamic cast - used with inheritance
		Player* playerPtr = dynamic_cast<Player*>(childPtr);
		if (playerPtr)
		{
			// this will ONLY run if childPtr is a player
			// if childPtr is NOT a player then 
			// playerPtr is nullptr
		}

		/// [help] how should I be thinking about putting this into GameObject
		// convert the child's location to world space
		// TODO : this should be a function on game object
		Vector2i testLocation = childRef.Location;
		testLocation += levelObjectPtr->Location;

		if (spawnLocation == testLocation)
		{
			foundObject = childPtr;
		}
	}*/

}

GameObject::~GameObject()
{
	// TODO: Clean up the children
}

void GameObject::SaveToFile(std::ofstream & saveFile)
{
	// save our location
	saveFile << GetType() << std::endl;
	saveFile << Location.X << "," << Location.Y << std::endl;
	saveFile << Name << std::endl;

	SaveClassSpecificData(saveFile);

	// save out all of the children
	saveFile << Children.size() << std::endl;
	for (GameObject* childPtr : Children)
	{
		childPtr->SaveToFile(saveFile);
	}
}

void GameObject::LoadFromFile(std::ifstream & loadFile)
{
	// load our location
	char dummyChar;
	loadFile >> Location.X >> dummyChar >> Location.Y;
	loadFile >> Name;

	LoadClassSpecificData(loadFile);

	// load in all of the children
	size_t numChildren = 0;
	loadFile >> numChildren;
	for (size_t count = 0; count < numChildren; ++count)
	{
		// read in the type of object
		int objectType = 0;
		loadFile >> objectType;

		GameObject* childPtr = nullptr;
		switch (objectType)
		{
		case egotBaseGameObject:
			childPtr = new GameObject();
			break;

		case egotPlayer:
			childPtr = new Player();
			break;

		case egotEnemy:
			childPtr = new Enemy();
			break;

		case egotWall:
			childPtr = new Wall();
			break;

		case egotDoor:
			childPtr = new Door();
			break;

		case egotKey:
			childPtr = new Key();
			break;

		case egotHealth:
			childPtr = new healthpack();
			break;
		}

		childPtr->LoadFromFile(loadFile);

		Children.push_back(childPtr);
		childPtr->Parent = this;


	}
}


void GameObject::Render(Gdiplus::Graphics & canvas)
{
	// Save the current transformation of the scene
	Gdiplus::Matrix transform;
	canvas.GetTransform(&transform);

	// offset our axes to our location
	canvas.TranslateTransform((Gdiplus::REAL)Location.X, (Gdiplus::REAL)Location.Y);

	/////////////////////////////////////////////////
	/// ALL DRAWING YOU DO FOR THIS GOES BETWEEN HERE
	for (GameObject* childPtr : Children)
		childPtr->Render(canvas);

	ClassSpecificRender(canvas);

	// Restore the previous transform
	canvas.SetTransform(&transform);
}

///Class specific functions [~virtual]

void GameObject::SaveClassSpecificData(std::ofstream & saveFile)
{
}

void GameObject::LoadClassSpecificData(std::ifstream & loadFile)
{
}

void GameObject::ClassSpecificRender(Gdiplus::Graphics & canvas)
{
}


void GameObject::Update(double deltaTime)
{
}
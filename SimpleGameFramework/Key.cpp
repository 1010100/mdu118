#include "stdafx.h"
#include "Key.h"


Key::Key()
{
}


Key::~Key()
{
}

void Key::SaveClassSpecificData(std::ofstream & saveFile)
{
	saveFile << this->isKeyUsed << std::endl;
}

void Key::LoadClassSpecificData(std::ifstream & loadFile)
{
	loadFile >> isKeyUsed;
}

void Key::ClassSpecificRender(Gdiplus::Graphics & canvas)
{
	//DebugLog("attempting to draw Key");
	GameFrameworkInstance.DrawRectangle(canvas, AABBi(Vector2i(0, 0), Vector2i(TileSize, TileSize)), true, Gdiplus::Color::Yellow);
}
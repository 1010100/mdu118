#pragma once
#include "GameManager.h"
#include "GameObject.h"

class healthpack : public GameObject
{
public:
	healthpack();
	virtual ~healthpack();

	virtual GameObjectType GetType()
	{
		return egotHealth;
	}

	virtual EnumInteraction Interact()
	{
		return interactionHeal;
	}

	virtual void SaveClassSpecificData(std::ofstream& saveFile);
	virtual void LoadClassSpecificData(std::ifstream& loadFile);
	virtual void ClassSpecificRender(Gdiplus::Graphics & canvas);


public:


};


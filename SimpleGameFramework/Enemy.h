#pragma once

#include "GameObject.h"
#include "GameManager.h"

class Enemy : public GameObject
{

public:
	Enemy();
	virtual ~Enemy();

	virtual GameObjectType GetType()
	{
		return egotEnemy;
	}

	virtual EnumInteraction Interact()
	{
		return interactionHarm;
	}

	virtual void SaveClassSpecificData(std::ofstream& saveFile);
	virtual void LoadClassSpecificData(std::ifstream& loadFile);
	virtual void ClassSpecificRender(Gdiplus::Graphics & canvas);


public:

	int enemyDamage = 1;
	
};


#include "stdafx.h"
#include "GameInput.h"
#include "GameManager.h"

GameInput& GameInput::Instance()
{
	static GameInput instance;

	return instance;
}

GameInput::GameInput()
{
}

GameInput::~GameInput()
{
}

void GameInput::BeginPlay()
{

}

void GameInput::EndPlay()
{

}

void GameInput::Update(double deltaTime)
{
	Vector2i movementDirection = Vector2i::Zero;

	// The code below polls individual keys to determine if they are currently down.
	if (GetKeyState('W') & 0x8000)
	{
		movementDirection += Vector2i(0, -1);
	}
	if (GetKeyState('A') & 0x8000)
	{
		movementDirection += Vector2i(-1, 0);
	}
	if (GetKeyState('S') & 0x8000)
	{
		movementDirection += Vector2i(0, 1);
	}
	if (GetKeyState('D') & 0x8000)
	{
		movementDirection += Vector2i(1, 0);
	}

	GameManagerInstance.ApplyMovement(movementDirection);
}

void GameInput::OnKeyDown(UINT keyCode, UINT repeatCount)
{
	// NOTE: This method will not detect multiple simultaneous key presses.
	// To detect simultaneous presses you must use GetKeyState and check
	// each key of interest.

	switch (keyCode)
	{
	case '1':
		DebugLog("User pressed 1. attempting to place player");
		GameManagerInstance.PlacementSelector(egotPlayer);
		break;

	case '2':
		DebugLog("User pressed 2. attempting to place enemy");
		GameManagerInstance.PlacementSelector(egotEnemy);
		break;

	case '3':
		DebugLog("User pressed 3. attempting to place wall");
		GameManagerInstance.PlacementSelector(egotWall);
		break;

	case '4':
		DebugLog("User pressed 4. attempting to place Door");
		GameManagerInstance.PlacementSelector(egotDoor);
		break;

	case '5':
		DebugLog("User pressed 5. attempting to place Key");
		GameManagerInstance.PlacementSelector(egotKey);
		break;

	case '6':
		DebugLog("User pressed 6. attempting to place Health");
		GameManagerInstance.PlacementSelector(egotHealth);
		break;

	case 'W':
		break;
	case 'A':
		break;
	case 'S':
		break;
	case 'D':
		break;
	case 'Q':
		break;
	case 'E':
		break;
	case 'F':
		break;
	case 'C':
		break;

	case VK_LEFT:
		break;
	case VK_RIGHT:
		break;
	case VK_UP:
		break;
	case VK_DOWN:
		break;

	case VK_CONTROL:
		break;
	case VK_SPACE:
		break;
	case VK_SHIFT:
		break;

	case VK_F1:
		break;
	case VK_F2:
		break;
	case VK_F3:
		break;
	case VK_F4:
		break;
	case VK_F5:
		break;
	case VK_F6:
		break;
	case VK_F7:
		break;
	case VK_F8:
		break;
	case VK_F9:
		break;
	case VK_F10:
		break;
	case VK_F11:
		break;
	case VK_F12:
		break;
	}
}

void GameInput::OnKeyUp(UINT keyCode, UINT repeatCount)
{
	// NOTE: This method will not detect multiple simultaneous key presses.
	// To detect simultaneous presses you must use GetKeyState and check
	// each key of interest.

	switch (keyCode)
	{
	case 'W':
		break;
	case 'A':
		break;
	case 'S':
		break;
	case 'D':
		break;
	case 'Q':
		break;
	case 'E':
		break;
	case 'F':
		break;
	case 'C':
		break;

	case VK_LEFT:
		break;
	case VK_RIGHT:
		break;
	case VK_UP:
		break;
	case VK_DOWN:
		break;

	case VK_CONTROL:
		break;
	case VK_SPACE:
		break;
	case VK_SHIFT:
		break;

	case VK_F1:
		// toggles between editmode
		GameManagerInstance.ToggleEdit();
		break;

	case VK_F2:

		DebugLog("Pressed F1. attempting save function");
		GameManagerInstance.SaveLevel();

		break;
	case VK_F3:

		DebugLog("Pressed F2. attempting load function");
		GameManagerInstance.LoadLevel(1); // remove this junk
		break;
	case VK_F4:
		break;
	case VK_F5:
		break;
	case VK_F6:
		break;
	case VK_F7:
		break;
	case VK_F8:
		break;
	case VK_F9:
		break;
	case VK_F10:
		break;
	case VK_F11:
		break;
	case VK_F12:
		break;
	}
}

void GameInput::OnLButtonDown(const Vector2i& point)
{
	GameManagerInstance.LeftButtonDown(point);

	DebugLog("OnLButtonDown at " << point.X << "," << point.Y);
}

void GameInput::OnLButtonUp(const Vector2i& point)
{
	DebugLog("OnLButtonUp at " << point.X << "," << point.Y);
}

void GameInput::OnLButtonDblClk(const Vector2i& point)
{
	DebugLog("OnLButtonDblClk at " << point.X << "," << point.Y);
}

void GameInput::OnRButtonDown(const Vector2i& point)
{
	DebugLog("OnRButtonDown at " << point.X << "," << point.Y);
}

void GameInput::OnRButtonUp(const Vector2i& point)
{
	DebugLog("OnRButtonUp at " << point.X << "," << point.Y);
}

void GameInput::OnRButtonDblClk(const Vector2i& point)
{
	DebugLog("OnRButtonDblClk at " << point.X << "," << point.Y);
}

void GameInput::OnMouseMove(const Vector2i& point)
{
}

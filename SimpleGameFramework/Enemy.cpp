#include "stdafx.h"
#include "Enemy.h"


Enemy::Enemy()
{
}


Enemy::~Enemy()
{
}

void Enemy::SaveClassSpecificData(std::ofstream & saveFile)
{
	saveFile << this->enemyDamage << std::endl;
	//saveFile << this->enemyLocation << std::endl;
}

void Enemy::LoadClassSpecificData(std::ifstream & loadFile)
{
	loadFile >> enemyDamage;
	//loadFile >> enemyLocation;
}

void Enemy::ClassSpecificRender(Gdiplus::Graphics & canvas)
{
	//fix so it takes Tile size variable
	//DebugLog("attempting to draw Enemy");
	GameFrameworkInstance.DrawRectangle(canvas, AABBi(Vector2i(0, 0), Vector2i(TileSize, TileSize)), true, Gdiplus::Color::Red);
}

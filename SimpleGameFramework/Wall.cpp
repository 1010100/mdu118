#include "stdafx.h"
#include "Wall.h"


Wall::Wall()
{
}


Wall::~Wall()
{
}

void Wall::SaveClassSpecificData(std::ofstream & saveFile)
{
	//saveFile << this->wallLocation << std::endl;
	// is necessary? 
}

void Wall::LoadClassSpecificData(std::ifstream & loadFile)
{
	//loadFile >> this->wallLocation >> std::endl;
	// is necessary? 
}

void Wall::ClassSpecificRender(Gdiplus::Graphics & canvas)
{
	//fix so it takes Tile size variable
	//DebugLog("attempting to draw wall");
	GameFrameworkInstance.DrawRectangle(canvas, AABBi(Vector2i(0, 0), Vector2i(TileSize, TileSize)), true, Gdiplus::Color::IndianRed);
}

